package setRestrictions;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import static helpers.TestConstants.NEW_USERNAME;
import static helpers.TestConstants.NEW_PASSWORD;
import helpers.SetupHelper;

/*Verify if an user can Create a Page".

SETUP
1.Open the browser.
2.Open confluence website homepage.
3.Login using user credentials.
TEST
3. Click on Create a Page Button .
4. Enter the title of the Page 
5. Save the Page
6. Edit the restrictions on the page.
7. Save the restrictions on the page.
Verification :
6. LogOut from the session
7. Login with a different User 
8. Verify if the Page can be Viewed or Edited by the new user.

CLEANUP
8. Delete all the Pages that was created .
9. Close all instances of the browser .
*/

public class Pos001 extends SetupHelper {

	public String newFile = RandomStringUtils.randomAlphanumeric(10);
	public String editFile = RandomStringUtils.randomAlphanumeric(10);

	@DataProvider(name = "permissionData")
	public Object[][] createData1() {
		return new Object[][] { { "Editing restricted", "param1" + newFile, "param1" + editFile },
				{ "Viewing and editing restricted", "param2" + newFile, "param2" + editFile } };
	}

	@BeforeTest
	public void setup() throws Exception {
		super.setup();
	}

	@Test(dataProvider = "permissionData")
	public void test(String permission, String title, String edit) throws InterruptedException {

		System.out.println("*************Starting the Test (" + permission + ") ***************");

		pagesCreated.add(edit);
		dashboard.createPageButton().click();
		System.out.println("Successfully Clicked The Create Page Button");
		util.switchWindow();
		Assert.assertTrue(util.fillBlankPage(title), "Failed to edit the Page");
		createPage.saveButton().click();
		System.out.println("Successfully Edited the page");
		blankPage.restrictionsButton().click();
		System.out.println("Clicked The restrictions button");
		util.switchWindow();
		Assert.assertTrue(restrictionHelper.selectRestriction(permission),
				"Failed to select the restriction" + permission);
		System.out.println("Successfully selected the permission ->" + permission);

		Assert.assertTrue(
				verificationHelper.restrictionsVerification(title, edit, NEW_USERNAME, NEW_PASSWORD, permission),
				"Failed to verify that the restriction" + permission + "is applied");
		System.out.println("Successfully verified that the restriction " + permission + " is applied");

		System.out.println("*************Test Passed***************");
		System.out.println();

	}

	@AfterTest
	public void cleanUp() throws Exception {
		for (String page : pagesCreated) {
			util.deletePage(page);
			System.out.println("Successfully deleted  the page" + page);
		}
		super.cleanup();
	}
}
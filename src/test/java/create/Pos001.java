package create;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import helpers.SetupHelper;

/*Verify if an user can Create a Page".

SETUP
1.Open the browser.
2.Open confluence website homepage.
3.Login using user credentials.
TEST
3. Click on Create a Page Button .
4. Enter the title of the Page 
5. Save the Page
Verification :
6. Verify if the Page is Created in the Pages section 
7. Verify if the Page has the same title 

CLEANUP
8. Delete the Page that was created .
9. Close all instances of the browser .
*/

public class Pos001 extends SetupHelper {

	public String newFile = RandomStringUtils.randomAlphanumeric(10);
	public String editFile = RandomStringUtils.randomAlphanumeric(10);

	@BeforeTest
	public void setup() throws Exception {
		super.setup();
	}

	@Test
	public void test() throws InterruptedException {

		System.out.println("*************Starting the Test***************");
		
		pagesCreated.add(newFile);
		dashboard.createPageButton().click();
		System.out.println("Successfully Clicked The Create Page Button");
		util.switchWindow();
		Assert.assertTrue(util.fillBlankPage(newFile), "Failed to edit the Page");
		System.out.println("Successfully Edited the page");
		createPage.saveButton().click();
		System.out.println("Successfully created a new page");
		Assert.assertTrue(verificationHelper.viewVerification(newFile), "Verification Failed");
		System.out.println("Successfully verified that the page has been created");
		
		System.out.println("*************Test Passed***************");

	}

	@AfterTest
	public void cleanUp() throws Exception {
		for (String page : pagesCreated) {
			util.deletePage(page);
		}
		super.cleanup();
	}
}
package helpers;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import objectRepository.BlankPage;
import objectRepository.CreatePage;
import objectRepository.DashBoard;
import objectRepository.LoginPage;
import objectRepository.TemplatePage;

public class Utility {

	public WebDriver driver = null;
	public TemplatePage templatePage = null;
	private BlankPage blankPage = null;
	private CreatePage createPage = null;
	private LoginPage loginPage = null;
	private DashBoard dashBoard = null;

	public Utility(WebDriver driver) {
		this.driver = driver;
	}

	public void switchWindow() {
		for (String winHandle2 : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle2);
			// switch focus of WebDriver to the next found window handle
		}
	}

	public void createAndSwitch() {
		templatePage = new TemplatePage(driver);
		templatePage.createButton().click();
		switchWindow();
	}

	public boolean fillBlankPage(String title) {
		blankPage = new BlankPage(driver);
		blankPage.contentTitle().sendKeys(title);
		return true;
	}

	public void login(WebDriver driver, String url, String userName, String Password) {
		driver.get(url);
		loginPage = new LoginPage(driver);
		loginPage.username().sendKeys(userName);
		loginPage.password().sendKeys(Password);
		loginPage.login().submit();

	}

	public void logout(WebDriver driver) throws InterruptedException {
		dashBoard = new DashBoard(driver);
		Thread.sleep(10000);
		dashBoard.userMenuLink().click();		
		dashBoard.logoutLink().click();
		dashBoard.logoutButton().click();

	}

	public boolean selectRestrictions(String elementXpath, String restriction) {
		WebElement select2Element = driver.findElement(By.xpath(elementXpath));
		select2Element.click();
		WebDriverWait webDriverWait = new WebDriverWait(driver, 90);
		webDriverWait
				.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//ul[@class='select2-results']//div")));
		WebElement select2ElementResults = driver
				.findElement(By.xpath("//div[@id='select2-drop']/ul[@class='select2-results']"));
		List<WebElement> options = select2ElementResults.findElements(By.tagName("div"));
		for (WebElement option : options) {
			if (option.getText().contains(restriction)) {
				option.click();
				break;
			}
		}
		return true;
	}

	public void deletePage(String name) {
		createPage = new CreatePage(driver);
		dashBoard = new DashBoard(driver);
		try {
			createPage.pagesButton().click();
		} catch (NoSuchElementException e) {
			System.out.println("The session is logged out and pages button will not be in found ");
			dashBoard.spaces().click();

		}
		if (driver.findElement(By.linkText(name)).isDisplayed()) {
			driver.findElement(By.linkText(name)).click();
			dashBoard.actionMenuLink().click();
			dashBoard.deleteLink().click();
			dashBoard.confirmButton().click();
		}
	}

}

package helpers;

public class TestConstants {

	public static final String NO_RESTRICTIONS = "No restrictions";
	public static final String EDITING_RESTRICTED = "Editing restricted";
	public static final String VIEWING_AND_EDITING_RESTRICTED = "Viewing and editing restricted";
	public static final String BASE_URL = "https://test-exercise.atlassian.net/wiki/#all-updates";
	public static final String USERNAME = "abc";
	public static final String PASSWORD = "def";
	public static final String NEW_USERNAME = "abc";
	public static final String NEW_PASSWORD = "def";

	
}

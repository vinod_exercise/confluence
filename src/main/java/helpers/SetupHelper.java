package helpers;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import objectRepository.BlankPage;
import objectRepository.CreatePage;
import objectRepository.DashBoard;
import objectRepository.RestrictionsPage;
import objectRepository.TemplatePage;
import static helpers.TestConstants.BASE_URL;
import static helpers.TestConstants.USERNAME;
import static helpers.TestConstants.PASSWORD;;
//Helper Class which has methods which is  used in dealing with Setup

public class SetupHelper {

	public WebDriver driver;
	public DashBoard dashboard = null;
	public TemplatePage templatePage = null;
	public Utility util = null;
	private DesiredCapabilities capabilites = null;
	public RestrictionsPage restrictions = null;
	public BlankPage blankPage = null;
	public CreatePage createPage = null;
	public VerificationHelper verificationHelper = null;
	public RestrictionHelper restrictionHelper = null;
	public List<String> pagesCreated = null;

	// Helper method which can be used before every test to set up the
	// environment
	public void setup() throws Exception {
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		util = new Utility(driver);
		util.login(driver, BASE_URL, USERNAME, PASSWORD);
		dashboard = new DashBoard(driver);
		templatePage = new TemplatePage(driver);
		createPage = new CreatePage(driver);
		verificationHelper = new VerificationHelper(driver);
		restrictionHelper = new RestrictionHelper(driver);
		blankPage = new BlankPage(driver);
		util = new Utility(driver);
		restrictions = new RestrictionsPage(driver);
		pagesCreated = new ArrayList<String>();
	}

	public void cleanup() {
		if (driver != null)
			driver.quit();
	}

	// Helper method which can be used before every test to set up the parallel
	// environment

	// TODO
	// Write tests that use this functionality to test on different browsers.

	public void parallelSetup(String browser) throws MalformedURLException {

		if (browser.equalsIgnoreCase("firefox")) {
			new DesiredCapabilities();
			capabilites = DesiredCapabilities.firefox();
			capabilites.setBrowserName(browser);
			capabilites.setPlatform(Platform.WINDOWS);

		}

		else if (browser.equalsIgnoreCase("ie")) {
			new DesiredCapabilities();
			capabilites = DesiredCapabilities.internetExplorer();
			capabilites.setBrowserName(browser);
			capabilites.setPlatform(Platform.WINDOWS);

		}

		else if (browser.equalsIgnoreCase("chrome")) {
			new DesiredCapabilities();
			capabilites = DesiredCapabilities.chrome();
			capabilites.setBrowserName(browser);
			capabilites.setPlatform(Platform.WINDOWS);
		}
		URL url = new URL("http://localhost:4444/wd/hub");
		driver = new RemoteWebDriver(url, capabilites);
		driver.get("http://www.imdb.com");

	}
}

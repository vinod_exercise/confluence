package helpers;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import objectRepository.BlankPage;
import objectRepository.CreatePage;
import objectRepository.DashBoard;
import objectRepository.TemplatePage;
import static helpers.TestConstants.EDITING_RESTRICTED;
import static helpers.TestConstants.VIEWING_AND_EDITING_RESTRICTED;

public class VerificationHelper {

	public WebDriver driver = null;
	public TemplatePage templatePage = null;
	private CreatePage createPage = null;
	private BlankPage blankPage = null;
	private DashBoard dashBoard = null;
	private Utility util = null;
	private String baseUrl = "https://test-exercise.atlassian.net/wiki/#all-updates";

	public VerificationHelper(WebDriver driver) {
		this.driver = driver;
	}

	public boolean viewVerification(String title) throws InterruptedException {

		createPage = new CreatePage(driver);
		try {
			createPage.pagesButton().click();
		} catch (NoSuchElementException e) {
			System.out.println("The session is logged out and pages button will not be in found ");
			driver.findElement(By.xpath(".//*[@id='sidebar-spaces']/div/ul/li/a")).click();

		}
		driver.navigate().refresh();
		driver.findElement(By.linkText(title)).click();
		Assert.assertEquals(createPage.titleText().getText(), title);
		return true;
	}

	public boolean editVerification(String edit, String title) throws InterruptedException {
		createPage = new CreatePage(driver);
		blankPage = new BlankPage(driver);
		dashBoard = new DashBoard(driver);
		dashBoard.spaces().click();
		driver.navigate().refresh();
		Thread.sleep(10000);
		driver.findElement(By.linkText(title)).click();
		Thread.sleep(10000);
		blankPage.editPageLink().click();
		blankPage.contentTitle().clear();
		blankPage.contentTitle().sendKeys(edit);
		Thread.sleep(10000);
		createPage.saveButton().click();
		return true;
	}

	public boolean restrictionsVerification(String title, String edit, String userName, String password,
			String permission) throws InterruptedException {
		util = new Utility(driver);
		util.switchWindow();
		util.logout(driver);
		util.login(driver, baseUrl, userName, password);
		createPage = new CreatePage(driver);
		switch (permission) {

		case EDITING_RESTRICTED:
			// Verifies editing and viewing with new user 
			Assert.assertFalse(editVerification(edit, title),
					"Edited the Page on which the restrictions has been applied");
			Assert.assertTrue(viewVerification(edit), "Cannot View the Page");
			break;
		case VIEWING_AND_EDITING_RESTRICTED:
			// Verifies editing and viewing with new user 
			Assert.assertFalse(editVerification(edit, title),
					"Edited the Page on which the restrictions has been applied");
			Assert.assertFalse(viewVerification(edit), "Opened the Page on which the restrictions has been applied");
			break;
		}
		return true;
	}

}

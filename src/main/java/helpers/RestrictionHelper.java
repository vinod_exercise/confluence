package helpers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import objectRepository.CreatePage;
import objectRepository.RestrictionsPage;
import objectRepository.TemplatePage;

public class RestrictionHelper {

	public WebDriver driver = null;
	public TemplatePage templatePage = null;
	private Utility util = null;
	private RestrictionsPage restrictionsPage = null;
	private CreatePage createPage = null;

	public RestrictionHelper(WebDriver driver) {
		this.driver = driver;
	}

	public boolean selectRestriction(String restriction) {
		util = new Utility(driver);
		createPage = new CreatePage(driver);
		restrictionsPage = new RestrictionsPage(driver);
		Assert.assertTrue(util.selectRestrictions(".//*[@id='s2id_page-restrictions-dialog-selector']/a", restriction),
				"Selected Viewing and editing restricted restriction");
		restrictionsPage.pageRestrictionsAddButton().click();
		util.switchWindow();
		restrictionsPage.saveButton().click();
		System.out.println("Successfully Created the  page with restrictions");		
		return true;
	}

}

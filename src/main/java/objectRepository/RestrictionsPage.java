package objectRepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RestrictionsPage {

	public WebDriver driver = null;

	public RestrictionsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = ".//*[@id='select2-drop-mask']")
	WebElement restricionsDropButton;

	@FindBy(xpath = "//span[@class='No restrictions']")
	WebElement noRestrictions;

	@FindBy(xpath = "//span[@class='Editing restricted']")
	WebElement editingRestricted;

	@FindBy(xpath = "//span[@class='Viewing and editing restricted']")
	WebElement viewingAndEditingRestricted;

	@FindBy(id = "page-restrictions-dialog-save-button")
	WebElement saveButton;

	@FindBy(id = "restrictions-dialog-auto-picker")
	WebElement userAddField;

	@FindBy(id = "page-restrictions-permission-selector")
	WebElement selectRestrictionOption;

	@FindBy(id = "page-restrictions-add-button")
	WebElement pageRestrictionsAddButton;

	public WebElement restrictionsDropButton() {
		return restricionsDropButton;
	}

	public WebElement noRestrictions() {
		return noRestrictions;
	}

	public WebElement editingRestricted() {
		return editingRestricted;
	}

	public WebElement viewingAndEditingRestricted() {
		return viewingAndEditingRestricted;
	}

	public WebElement saveButton() {
		return saveButton;
	}

	public WebElement userAddField() {
		return userAddField;
	}

	public WebElement selectRestrictionOption() {
		return selectRestrictionOption;
	}

	public WebElement pageRestrictionsAddButton() {
		return pageRestrictionsAddButton;
	}

}

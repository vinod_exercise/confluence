package objectRepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CreatePage {

	public WebDriver driver = null;

	public CreatePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(css = "a[data-collapsed-tooltip='Pages']")
	WebElement pagesLink;

	@FindBy(id = "content-metadata-page-restrictions")
	WebElement restrictionsButton;

	@FindBy(id = "editPageLink")
	WebElement editPageLink;

	@FindBy(id = "title-text")
	WebElement titleText;

	@FindBy(id = "rte-button-publish")
	WebElement saveButton;

	public WebElement pagesButton() {
		return pagesLink;
	}

	public WebElement restrictionsButton() {
		return restrictionsButton;
	}

	public WebElement editPageLink() {
		return editPageLink;
	}

	public WebElement titleText() {
		return titleText;
	}

	public WebElement saveButton() {
		return saveButton;
	}

}

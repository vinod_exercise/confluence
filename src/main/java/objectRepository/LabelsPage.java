package objectRepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LabelsPage {

	public WebDriver driver = null;

	public LabelsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "labels-string")
	WebElement labelsString;

	@FindBy(id = "add-labels-editor-button")
	WebElement addlabelsButton;

	@FindBy(linkText = "Help")
	WebElement helpLink;

	@FindBy(linkText = "Close")
	WebElement closeLink;

	public WebElement labelsString() {
		return labelsString;
	}

	public WebElement addlabelsButton() {
		return addlabelsButton;
	}

	public WebElement helpLink() {
		return helpLink;
	}

	public WebElement closeLink() {
		return closeLink;
	}

}

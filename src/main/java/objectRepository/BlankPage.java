package objectRepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BlankPage {

	public WebDriver driver = null;

	public BlankPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "content-title")
	WebElement contentTitle;

	@FindBy(xpath = ".//*[@id='content-metadata-page-restrictions']")
	WebElement restrictionsButton;

	@FindBy(id = "rte-button-labels")
	WebElement labelsButton;

	@FindBy(id = "rte-button-location")
	WebElement locationButton;

	@FindBy(id = "editPageLink")
	WebElement editPageLink;

	public WebElement contentTitle() {
		return contentTitle;
	}

	public WebElement restrictionsButton() {
		return restrictionsButton;
	}

	public WebElement labelsButton() {
		return labelsButton;
	}

	public WebElement locationButton() {
		return locationButton;
	}

	public WebElement editPageLink() {
		return editPageLink;
	}

}

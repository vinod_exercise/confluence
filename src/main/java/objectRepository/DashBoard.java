package objectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DashBoard {

	public WebDriver driver = null;

	public DashBoard(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "quick-create-page-button")
	WebElement quickCreatePageButton;

	@FindBy(xpath = ".//*[@id='sidebar-spaces']/div/ul/li/a")
	WebElement spaces;

	@FindBy(xpath = ".//*[@id='action-remove-content-link']")
	WebElement deleteLink;

	@FindBy(id = "create-page-button")
	WebElement createPageButton;

	@FindBy(id = "logout-link")
	WebElement logoutLink;

	@FindBy(id = "logout")
	WebElement logoutButton;

	@FindBy(id = "action-menu-link")
	WebElement actionMenuLink;

	@FindBy(xpath = ".//*[@id='user-menu-link']")
	WebElement userMenuLink;

	@FindBy(id = "confirm")
	WebElement confirmButton;

	public WebElement createPageButton() {
		return quickCreatePageButton;
	}

	public WebElement logoutLink() {
		return logoutLink;
	}

	public WebElement logoutButton() {
		return logoutButton;
	}

	public WebElement spaces() {
		return spaces;
	}

	public WebElement deleteLink() {
		return deleteLink;
	}

	public WebElement actionMenuLink() {
		return actionMenuLink;
	}

	public WebElement userMenuLink() {
		return userMenuLink;
	}

	public WebElement confirmButton() {
		return confirmButton;
	}

}

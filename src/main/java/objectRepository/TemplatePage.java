package objectRepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TemplatePage {

	public WebDriver driver = null;

	public TemplatePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//div[contains(@class, 'template-name') and text()='Blank page']")
	WebElement blankPage;

	@FindBy(xpath = "//div[contains(@class, 'template-name') and text()='Blog post']")
	WebElement blogPost;

	@FindBy(xpath = "//div[contains(@class, 'template-name') and text()='Ask a question']")
	WebElement askQuestion;

	@FindBy(xpath = "//div[contains(@class, 'template-name') and text()='Calendar']")
	WebElement calendar;

	@FindBy(xpath = "//div[contains(@class, 'template-name') and text()='Decision']")
	WebElement decision;

	@FindBy(xpath = "//div[contains(@class, 'template-name') and text()='File list']")
	WebElement fileList;

	@FindBy(xpath = "//div[contains(@class, 'template-name') and text()='How-to article']")
	WebElement howToArticle;

	@FindBy(xpath = "//div[contains(@class, 'template-name') and text() = 'JIRA report']")
	WebElement jiraReport;

	@FindBy(xpath = "//div[contains(@class, 'tepmlate-name') and text()='Meeting notes']")
	WebElement meetingNotes;

	@FindBy(xpath = "//div[contains(@class, 'template-name') and text() = 'Product requirements']")
	WebElement productRequirements;

	@FindBy(xpath = "//div[contains(@class, 'template-name') and text() = 'Retrospective']")
	WebElement retrospective;

	@FindBy(xpath = "//div[contains(@class, 'template-name') and text() = 'Share a link']")
	WebElement shareLink;

	@FindBy(xpath = "//div[contains(@class, 'template-name') and text() = 'Sprint plan']")
	WebElement sprintPlan;

	@FindBy(xpath = "//div[contains(@class, 'template-name') and text() = 'Task report']")
	WebElement taskReport;

	@FindBy(xpath = "//div[contains(@class, 'template-name') and text() = 'Troubleshooting article']")
	WebElement troubleshootingArticle;
	
	@FindBy(xpath = "//button[contains(text(),'Create')]")
	WebElement createButton;
	
	@FindBy(linkText = "Close")
	WebElement closeLink;
	
	@FindBy(linkText = "Help")
	WebElement helpLink;
	
	@FindBy(id = "createDialogFilter")
	WebElement dialogFilter;
	
	public WebElement blankPage() {
		return blankPage;
	}

	public WebElement blogPost() {
		return blogPost;
	}

	public WebElement askQuestion() {
		return askQuestion;
	}

	public WebElement calendar() {
		return calendar;
	}

	public WebElement decision() {
		return decision;
	}

	public WebElement fileList() {
		return fileList;
	}

	public WebElement howToArticle() {
		return howToArticle;
	}

	public WebElement jiraReport() {
		return jiraReport;
	}

	public WebElement meetingNotes() {
		return meetingNotes;
	}

	public WebElement productRequirements() {
		return productRequirements;
	}

	public WebElement retrospective() {
		return retrospective;
	}

	public WebElement shareLink() {
		return shareLink;
	}

	public WebElement sprintPlan() {
		return sprintPlan;
	}

	public WebElement taskReport() {
		return taskReport;
	}

	public WebElement troubleshootingArticle() {
		return troubleshootingArticle;
	}
	
	public WebElement createButton() {
		return createButton;
	}
	
	public WebElement closeLink() {
		return closeLink;
	}
	
	public WebElement helpLink() {
		return helpLink;
	}
	
	public WebElement dialogFilter() {
		return dialogFilter;
	}
	

}

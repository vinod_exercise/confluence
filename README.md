# README #

QA Assistance Exercise 

### What is this repository for? ###

* Excersie to test create and edit restrictions feature in Confluence
* 1.0

### How do I get set up? ###

* I have created this project using Selenium, Maven and TestNg
* You have to configure Maven and TestNg 

### How to run tests? ###

* After setting Maven , goto cmd prompt .
* Browse to the folder where you have the project saved.
* run "mvn test" command on the cmd prompt.
* Alternatively , you can import the project to eclipse and run the testNg Suite 


# Information #

1) You need two different users of Confluence to run the tests and the details of the user should be given in helpers.TestConstants.java . We need two users because the test se the restriction and log outs and we need a different user to verify whether the page can be accessed.

2) I have assumed the page creation is only quick page create , not the templates . The creation of other templates can also be covered but , verification of the data inside the templates is a huge project .

3) driver.manage().window().maximize(); has an issue with my selenium jars,which is a accepted bug .  It should work fine if using older versions of Selenium . If its not working please maximise once the test starts .

4) I have used helpers wherever needed to make the test Code more readable .

5) I have used Page Object Factory model instead of Page Object model since it makes the code more readable .

# IMPROVEMENTS THAT CAN BE DONE : #

1) Logging configuration can be done for better logging.

2) More data driven tests can be written .

3) Cross Browser testing can done.

4) Test can be extended to test the create page templates .

5) Use config.properties to get the different date needed to run the tests . 



 




### Who do I talk to? ###
Vinod: 

vinod.sirgaaon@gmail.com

0401585042